# Introduction

   <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(255,0,0,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
     <strong>WARNING:</strong><br/><br/>
     Please read the new section in order to choose a proper networks for your needs.
   </div>
[click here](/important/README.md)


This guide aims to provide a walk-through for setting up a rudimentary
virtual infrastructure in MetaCentrum Cloud. It is a good jumping-off
point for most users.

The left sidebar can be used for navigation throughout the documentation.
The whole guide can also be downloaded as PDFs for printing or later use.

__New users__ should head over to the [Get Access](/register/README.md)
section and make sure they have an active user account and required
permissions to access the service.

__Beginners__ should start in the [Quick Start](/quick-start/README.md)
section which provides a step-by-step guide for starting the first
virtual machine instance.

__Advanced users__ should continue in the [Advanced Features](/gui/README.md)
or [Command Line Interface](/cli/README.md) sections, as these are
more suitable for complex use cases and exploration of available
features.

__Expert users__ with complex infrastructural or scientific use cases
should contact user support and request assistance specifically for
their use case.

__Frequently asked questions__ and corresponding answers can be found in
the [FAQ](/faq/README.md) section. Please, consult this section before
contacting user support.

Bear in mind that this is not the complete documentation to OpenStack
but rather a quick guide that is supposed to help you with elementary
use of our infrastructure. If you need more information, please turn
to [the official documentation](https://docs.openstack.org/rocky/user/)
or contact user support and describe your use case.

Please visit [Network](/network/README.md) section in order to see how you should set up the network.
