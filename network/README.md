# Networking

For the networking in Cloud2 metacentrum we need to distinguish two different scenarios, the personal project and group project. Due to the fact that a personal projects are meant for getting to know the environment not for production ready services.

## Networking in the personal vs. group projects

### Personal Project networking

Is currently limited to the common internal network. The network in which you should start your machine is called `78-128-250-pers-proj-net` and is selected by default when using dashboard to start a machine (if you do not have another network created). The floaing IPs you need to access a virtual machine is `public-cesnet-78-128-250-PERSONAL`. Any other allocated FIP and `external gateway` will be deleted. You cannot use router with the personal project and any previously created routers will be deleted.

### Group project

In group project situation is rather different. You cannot use the same approach as personal project (resources allocated in previously mentioned networks will be periodically released). For FIP you need to allocate from pools with `-GROUP` suffix (namely `public-cesnet-78-128-250-PERSONAL` or `public-muni-147-251-124-GROUP`).

#### Network creation

For group project you need to create internal network first, you may use autoallocated pool for subnet autocreation.
Navigate yourself towards **Network &gt; Networks** in the left menu and click on the **Create Network** on the right side of the window. This will start an interactive dialog for network creation.
![](/network/images/1.png)
![](/network/images/2.png)
Inside the interactive dialog:
1. Type in the network name
![](/network/images/3.png)
2. Move to the **Subnet** section either by clicking next or by clicking on the **Subnet** tab. You may choose to enter network range manually (recommended for advanced users in order to not interfere with the public IP address ranges), or select **Allocate Network Address from a pool**. In the **Address pool** section select a `private-192-168`. Select Network mask which suits your needs (`27` as default can hold up to 29 machines, use IP calculator if you are not sure).
![](/network/images/4.png)
3. For the last tab **Subnet Details** just check that a DNS is present and DHCP box is checked, alternatively you can create the allocation pool or specify static routes in here (for advanced users).
![](/network/images/5.png)


#### Router creation

Navigate yourself towards **Network &gt; Routers** in the left menu and click on the **Create Router** on the right side of the window.
In the interactive dialog:
1. Enter router name and select external gateway with the `-GROUP` suffix.
![](/network/images/r1.png)

Now you need to attach your internal network to the router.
1. Click on the router you just created.
2. Move to the **Interfaces** tab and click on the **Add interface**.
![](/network/images/r2.png)
3. Select a previously created subnet and submit.
![](/network/images/r3.png)

#### FIP allocation

Navigate yourself towards **Network &gt; Floating IPs** in the left menu and click on the **Allocate IP To Project** on the right side of the window.
Select the same public network with `-GROUP` suffix as you did with the router. This is cruicial in order to network connection function correctly.
![](/network/images/f1.png)

## Change external network in GUI

Following chapter covers the problem of changing the external network via GUI or CLI.

### Existing Floating IP release

First you need to release existing Floating IPs from your instances - go to **Project &gt; Compute &gt;  Instances**. Click on the menu **Actions** on the instance you whish to change and **Disassociate Floating IP** and specify that you wish to **Release Floating IP** WARN: After this action your project will no longer be able to use the floating IP address you released. Confirm that you wish to disassociate the floating IP by clicking on the **Disassociate** button. When you are done with all instances connected to your router you may continue with the next step.
![](/network/images/instance1.png)

### Clear Gateway

Now, you should navigate yourself to the **Project &gt; Network &gt; Routers**. Click on the action **Clear Gateway** of your router. This action will disassociate the external network from your router, so your machines will not longer be able to access Internet. If you get an error go back to step 1 and **Disassociate your Floating IPs**.
![](/network/images/clear-router1.png)

### Set Gateway

1. Now, you can set your gateway by clicking **Set Gateway**.
![](/network/images/set-router1.png)

2. Choose network you desire to use (e.g. **public-cesnet-78-128-251**) and confirm.
![](/network/images/set-router2.png)

### Allocate new Floating IP(s)

1. Go to **Project &gt; Network &gt; Floating IPs** and click on the **Allocate IP to Project** button. Select **Pool** with the same value as the network you chose in the previous step and confirm it by clicking **Allocate IP**
![](/network/images/allocate-fip.png)

2. Now click on the **Associate** button next to the Floating IP you just created. Select **Port to be associated** with desired instance. Confirm with the **Associate** button. Repeat this section for all your machines requiring a Floating IP.
![](/network/images/associate-fip.png)

##  Change external network CLI


### Remove existing floating ip

1. List your servers:

```
$ openstack server list
+--------------------------------------+-----------+--------+-------------------------------------------------------+-------+----------------+
| ID                                   | Name      | Status | Networks                                              | Image | Flavor         |
+--------------------------------------+-----------+--------+-------------------------------------------------------+-------+----------------+
| 1a0d4624-5294-425a-af37-a83eb0640e1c | net-test1 | ACTIVE | auto_allocated_network=192.168.8.196, 147.251.124.248 |       | standard.small |
+--------------------------------------+-----------+--------+-------------------------------------------------------+-------+----------------+
```

2. remove floating ips:

```
$ openstack server remove floating ip  net-test 147.251.124.248
$ openstack floating ip delete 147.251.124.248
```

### Clear gateway

1. Find your router:
```
$  openstack router list
+--------------------------------------+-----------------------+--------+-------+-------------+------+----------------------------------+
| ID                                   | Name                  | Status | State | Distributed | HA   | Project                          |
+--------------------------------------+-----------------------+--------+-------+-------------+------+----------------------------------+
| 0bd0374d-b62e-429a-8573-3e8527399b68 | auto_allocated_router | ACTIVE | UP    | None        | None | f0c339b86ddb4699b6eab7acee8d4508 |
+--------------------------------------+-----------------------+--------+-------+-------------+------+----------------------------------+
```

2. Verify:
```
$ openstack router show 0bd0374d-b62e-429a-8573-3e8527399b68
+-------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Field                   | Value                                                                                                                                                                                       |
+-------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| admin_state_up          | UP                                                                                                                                                                                          |
| availability_zone_hints | None                                                                                                                                                                                        |
| availability_zones      | None                                                                                                                                                                                        |
| created_at              | 2019-06-06T04:47:15Z                                                                                                                                                                        |
| description             | None                                                                                                                                                                                        |
| distributed             | None                                                                                                                                                                                        |
| external_gateway_info   | {"network_id": "8d5e18ab-5d43-4fb5-83e9-eb581c4d5365", "enable_snat": true, "external_fixed_ips": [{"subnet_id": "41e0cd1c-5ab8-465f-8605-2e7d6a3fe5b4", "ip_address": "147.251.124.177"}]} |
| flavor_id               | None                                                                                                                                                                                        |
| ha                      | None                                                                                                                                                                                        |
| id                      | 0bd0374d-b62e-429a-8573-3e8527399b68                                                                                                                                                        |
| interfaces_info         | [{"port_id": "92c3f6fe-afa8-47c6-a1a6-f6a1b3c54f72", "ip_address": "192.168.8.193", "subnet_id": "e903d5b9-ac90-4ca8-be2c-c509a0153982"}]                                                   |
| location                | Munch({'cloud': '', 'region_name': 'brno1', 'zone': None, 'project': Munch({'id': 'f0c339b86ddb4699b6eab7acee8d4508', 'name': None, 'domain_id': None, 'domain_name': None})})              |
| name                    | auto_allocated_router                                                                                                                                                                       |
| project_id              | f0c339b86ddb4699b6eab7acee8d4508                                                                                                                                                            |
| revision_number         | 24                                                                                                                                                                                          |
| routes                  |                                                                                                                                                                                             |
| status                  | ACTIVE                                                                                                                                                                                      |
| tags                    |                                                                                                                                                                                             |
| updated_at              | 2019-06-06T06:34:34Z                                                                                                                                                                        |
+-------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
```
3. Unset gateway (by ID of the router):

```
$ openstack router unset --external-gateway 0bd0374d-b62e-429a-8573-3e8527399b68
```

### Set Gateway

1. Choose a new external network:

```
$ openstack network list
+--------------------------------------+--------------------------+--------------------------------------+
| ID                                   | Name                     | Subnets                              |
+--------------------------------------+--------------------------+--------------------------------------+
| 410e1b3a-1971-446b-b835-bf503917680d | public-cesnet-78-128-251 | 937106e2-3d51-43cc-83b6-c779465011e5 |
| 8d5e18ab-5d43-4fb5-83e9-eb581c4d5365 | public-muni-147-251-124  | 41e0cd1c-5ab8-465f-8605-2e7d6a3fe5b4 |
| c708270d-0545-4be2-9b8f-84cf75ce09cf | auto_allocated_network   | e903d5b9-ac90-4ca8-be2c-c509a0153982 |
| d896044f-90eb-45ee-8cb1-86bf8cb3f9fe | private-muni-10-16-116   | 3d325abf-f9f8-4790-988f-9cd3d1dea4f3 |
+--------------------------------------+--------------------------+--------------------------------------+
```

2. Set the new external network for the router

```
$ openstack router set --external-gateway public-cesnet-78-128-251  0bd0374d-b62e-429a-8573-3e8527399b68
```

### Allocate new Floating IP(s):

1. Allocate new Floating IPs:

```
$ openstack floating ip create public-cesnet-78-128-251
+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Field               | Value                                                                                                                                                                          |
+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| created_at          | 2019-06-06T06:56:51Z                                                                                                                                                           |
| description         |                                                                                                                                                                                |
| dns_domain          | None                                                                                                                                                                           |
| dns_name            | None                                                                                                                                                                           |
| fixed_ip_address    | None                                                                                                                                                                           |
| floating_ip_address | 78.128.251.27                                                                                                                                                                  |
| floating_network_id | 410e1b3a-1971-446b-b835-bf503917680d                                                                                                                                           |
| id                  | d054b6b3-bbd3-485d-a46b-b80682df8fc8                                                                                                                                           |
| location            | Munch({'cloud': '', 'region_name': 'brno1', 'zone': None, 'project': Munch({'id': 'f0c339b86ddb4699b6eab7acee8d4508', 'name': None, 'domain_id': None, 'domain_name': None})}) |
| name                | 78.128.251.27                                                                                                                                                                  |
| port_details        | None                                                                                                                                                                           |
| port_id             | None                                                                                                                                                                           |
| project_id          | f0c339b86ddb4699b6eab7acee8d4508                                                                                                                                               |
| qos_policy_id       | None                                                                                                                                                                           |
| revision_number     | 0                                                                                                                                                                              |
| router_id           | None                                                                                                                                                                           |
| status              | DOWN                                                                                                                                                                           |
| subnet_id           | None                                                                                                                                                                           |
| tags                | []                                                                                                                                                                             |
| updated_at          | 2019-06-06T06:56:51Z                                                                                                                                                           |
+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
```

2. And assign it to your server:

```
$ openstack server add floating ip  net-test1 78.128.251.27
```
